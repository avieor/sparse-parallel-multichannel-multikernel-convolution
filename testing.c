#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <assert.h>
#include <omp.h>
#include <math.h>
#include <stdint.h>
#include <xmmintrin.h>

#define SCALAR_MAX 2048
#define BY4_MASK 3;

long long mul_time;
int width, height, nkernels;
struct timeval start_time;
struct timeval stop_time;


int optimised(int nkernels, int height, int width){
    float *** output;
    /* record starting time of team's code*/
   
    gettimeofday(&start_time, NULL);

    int m, h, w;
    int remainderBy4 = width & BY4_MASK;
    if(width * height * nkernels <= SCALAR_MAX){ omp_set_num_threads(1); }
    __m128 v = _mm_set1_ps(0.0);
    
    #pragma omp parallel for collapse(2)
    //initialize the output matrix to zero
    for ( m = 0; m < nkernels; m++ ) {
      for ( h = 0; h < height; h++ ) {
        for(w = 0; w < remainderBy4; w++)
        {
            output[m][h][w] = 0.0; //scalar
        }
        while(w < width)
        {
            _mm_store_ps(&output[m][h][w], v); //vector
            w+=4;
        }
      }
    }  
    /* record finishing time */
    gettimeofday(&stop_time, NULL);
    mul_time = (stop_time.tv_sec - start_time.tv_sec) * 1000000L +
    (stop_time.tv_usec - start_time.tv_usec);
    printf("Optimised time: %lld microseconds\n", mul_time);
    return 0;
}

int unoptimised(int nkernels, int height, int width){
    float *** output;
    int m, h, w;

    /* record starting time of team's code*/
    gettimeofday(&start_time, NULL);
    // initialize the output matrix to zero
    
    for ( m = 0; m < nkernels; m++ ) {
        for ( h = 0; h < height; h++ ) {
            for ( w = 0; w < width; w++ ) {
                output[m][h][w] = 0.0;
            }
        }
    }
    /* record finishing time */
    gettimeofday(&stop_time, NULL);
    mul_time = (stop_time.tv_sec - start_time.tv_sec) * 1000000L +
    (stop_time.tv_usec - start_time.tv_usec);
    printf("Unoptimised time: %lld microseconds\n", mul_time);
    return 0;
}

int main(int argc, char ** argv){

    width = atoi(argv[1]);
    height = atoi(argv[2]);
    nkernels = atoi(argv[3]);

    unoptimised(nkernels, height, width);
    optimised(nkernels, height, width);

    return 0;
}